#include "AssetInstance.h"
/*****************************************getters/setters****************************************************/
Asset_instance::Asset_instance(Model * modelP, Material *  materialP)
{
	model = modelP;
	material = new Material[model->GetMeshCount()];



	for (unsigned int i = 0; i < modelP->GetMeshCount(); i++)
	{
		material[i] = materialP[i];
	}
	cout << " ok " << endl;
}



Asset_instance::Asset_instance(Model * modelP)
{
	model = modelP;

	vector<Texture * > TestTex;

	Material * Materials;

	Materials = new Material[model->GetMaterialsCount()];


	for (unsigned int i = 0; i < model->GetMaterialsCount(); i++)
	{
		Texture TempTex;
		string TexName = model->GetMaterial(i).diffuse_texname;
		if (TexName == "")
		{
			cout << "empity texname" << endl;
			TempTex.LoadTexFromDDS("uvtemplate.DDS");
		}
		else
		{
			TexName.insert(TexName.size() - 4, ".DDS");
			TexName.erase(TexName.size() - 4, 4);
			TempTex.LoadTexFromDDS(TexName);
		}

		TestTex.push_back(new Texture);
		*TestTex[i] = TempTex;
		Material TempMat;
		TempMat.SetTextures(TestTex[i], "Test");
		TempMat.MakeShader("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
		Materials[i] = TempMat;
	}


	material = new Material[model->GetMeshCount()];
	for (unsigned int i = 0; i < model->GetMeshCount(); i++)
	{
		material[i] = Materials[model->GetMaterialId(i, 0)];
	}

}

Model * Asset_instance::GetModel()
{
	return model;
}

Material * Asset_instance::getMaterial()
{
	return material;
}

void Asset_instance::SetMVP(mat4 MVPp)
{
	MVP = MVPp;
}

mat4 Asset_instance::getMVP()
{
	return MVP;
}
/*****************************************getters/setters****************************************************/

void Asset_instance::Asset_DrawAll()
{


	model->DrawAll(MVP, material);


}
