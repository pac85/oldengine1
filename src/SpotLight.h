#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H
#include"main.h"
#include"ShadowMap.h"
#include"Cone.h"
#include"GBuffer.h"
#include"Actor.h"

class SpotLight : Actor
{
    public:
        SpotLight(GLuint , GLuint , unsigned int, bool);
        void BindShadowMapForDrawing(GLuint);
        void DrawLight(GBuffer InputGBuffer, mat4 MVP);

        void set_position(vec3 _position);
        void set_rotation(vec3 _rotation);
        void SetIntensity(vec3 _Intensity);
        void SetSourceRad(float _SourceRad);
        void SetConeLength(float _ConeLength);
        void SetConeAngle(float _ConeAngle);
        void SetInternalConeAngle(float _IConeAngle);

        virtual ~SpotLight();

    protected:

    private:
        bool bShadows;
        ShadowMap * lShadowMap;
        GLuint SpotLightProgram, ConeProgram, MVPid, VPMid,
         PositionID, IntensityID, SourceRadID, DirectionID, ConeAngleCosID, IConeAngleCosID;
        vec3 Intensity, Direction, UpVec;
        mat4 VPM, ConeMatrix;
        float SourceRad, ConeLength, ConeAngle, IConeAngle;
        Cone BoundingCone;


        void tick(float delta);
        void GameStart();

};

#endif // SPOTLIGHT_H
