
#include"GBuffer.h"
#include "textures.h"


	GBuffer::GBuffer(int WindowWidth, int WindowHeight)
	{
	    WHeight = WindowHeight;
        WWidth = WindowWidth;
		//This generate the 7 texures of the G-Buffer (diffuse color, specular colour, world space coord, object's normal, normal map's normal, glossyness, emission) and the G-Bufer's frame buffer
		glGenFramebuffers(1, &GFrameBuffer);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GFrameBuffer);
		glGenTextures(9, GBufferTextures);

		for (int i = 0; i < 7; i++)
		{
			glBindTexture(GL_TEXTURE_2D, GBufferTextures[i]);
		//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WindowHeight, WindowWidth, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, WindowWidth, WindowHeight, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			//glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, GBufferTextures[i], 0);
            glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GBufferTextures[i], 0);
		}

        glGenTextures(1, &FinalImage);
        glBindTexture(GL_TEXTURE_2D, FinalImage);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, WindowWidth, WindowHeight, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT7, FinalImage, 0);

        //This generates the depth buffer and the linear depth buffer
        glBindTexture(GL_TEXTURE_2D, GBufferTextures[7]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH32F_STENCIL8, WindowWidth, WindowHeight, 0, GL_DEPTH_STENCIL, GL_FLOAT_32_UNSIGNED_INT_24_8_REV, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, GBufferTextures[8], 0);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GBufferTextures[7], 0);

        //Stencil buffer creation
        /*glGenRenderbuffers(1, &Stencil);
        glBindRenderbuffer(GL_RENDERBUFFER, Stencil);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, WindowWidth, WindowHeight);
        glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, Stencil);*/

		//GLenum DrawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6};
		//glDrawBuffers(7, DrawBuffers);
		//Checks te frame buffer state
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("error:Unable to create the G-Buffer's frame buffer\n");
			bIsOk = false;
		}
		GBuffertexturesNames[0] = "Diffuse_color";
		GBuffertexturesNames[1] = "Specular_color";
		GBuffertexturesNames[2] = "World_space_coords";
		GBuffertexturesNames[3] = "Normal";
		GBuffertexturesNames[4] = "Normal_normalmap";
		GBuffertexturesNames[5] = "Glossyness";
		GBuffertexturesNames[6] = "Emission_color";
		GBuffertexturesNames[7] = "Depth";


		glGenVertexArrays(1, &quadArray);
		glBindVertexArray(quadArray);

		static const GLfloat quad[] = {
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			-1.0f,  1.0f, 0.0f,
			-1.0f,  1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			1.0f,  1.0f, 0.0f,
		};

		glGenBuffers(1, &Quad);
		glBindBuffer(GL_ARRAY_BUFFER, Quad);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);

		//Unbinds TEXTURE_2D end GL_FRAMEBUFFER
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);


		//testTexture = loadDDS("uvtemplate.DDS");
		//GBufferTextures[0] = loadDDS("uvtemplate.DDS");

	}
	void GBuffer::BindGBufferForDrawing()
	{
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GFrameBuffer);

		GLenum DrawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6};
		glDrawBuffers(4, DrawBuffers);

		glViewport(0, 0, WWidth, WHeight);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void GBuffer::BindGBufferForReading()
	{
	    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GFrameBuffer);

	    glDrawBuffer(GL_NONE);

	    /*for (int i = 0; i <= 8; i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, GBufferTextures[i]);
			GLuint TexID = glGetUniformLocation(Program, GBuffertexturesNames[i].c_str());
			glUniform1i(TexID, i);
		}*/
	}

	void GBuffer::BindGBufferForLighting(GLuint Program)
	{
	    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GFrameBuffer);

	    glDrawBuffer(GL_COLOR_ATTACHMENT7);

	     for (int i = 0; i <= 8; i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, GBufferTextures[i]);
			GLuint TexID = glGetUniformLocation(Program, GBuffertexturesNames[i].c_str());
			glUniform1i(TexID, i);
		}
	}

	void GBuffer::CleanGBuffer()
	{
	    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GFrameBuffer);
	    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void GBuffer::DrawGBuffer(GLuint target, int Width, int Height, GLuint Program)
	{

		//sets various texures
		glBindFramebuffer(GL_FRAMEBUFFER, target);
		glViewport(0, 0, Width, Height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(Program);
		for (int i = 0; i <= 8; i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, GBufferTextures[i]);
			GLuint TexID = glGetUniformLocation(Program, GBuffertexturesNames[i].c_str());
			glUniform1i(TexID, i);
		}

		glActiveTexture(GL_TEXTURE0 + 8);
		glBindTexture(GL_TEXTURE_2D, FinalImage);
		GLuint TexID = glGetUniformLocation(Program, "LightingTexture");
		glUniform1i(TexID, 8);
       /* glActiveTexture(GL_TEXTURE0 );
		glBindTexture(GL_TEXTURE_2D, testTexture);
		GLuint TexID = glGetUniformLocation(Program, "Diffuse_color");
		glUniform1i(TexID, 0);*/

		glBindVertexArray(quadArray);
		//sends vertices to the shader and renders
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, Quad);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		//clean up
		glDisableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GBuffer::DrawFinalBuffer()
	{
	    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, GFrameBuffer);
        glReadBuffer(GL_COLOR_ATTACHMENT7);
        glBlitFramebuffer(0, 0, WWidth, WHeight,
                          0, 0, WWidth, WHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);
	}




