#ifndef SHADOWMAP_H
#define SHADOWMAP_H
#include"main.h"
#define CUBEMAP true
struct FaceInfo
{
    GLenum CubeFace;
    vec3 Target, upVec;
};

class ShadowMap
{
    public:
        ShadowMap(float, bool);
        virtual ~ShadowMap();
        void BindShadowMapForDrawing(vec3, float, float, unsigned int, GLuint);
        void BindShadowMapForDrawing(mat4, GLuint);
        void UseShadowMap(GLuint Program);
        mat4 GetVM(unsigned int);

    protected:


    private:
        GLuint DepthRB, ShadowMapTex, ShadowMapFBO;
        mat4 CSMMatrices[6];
        float nClip, fClip;
        vec3 pos;
        unsigned int Res;
        bool bCubeMap;


};

#endif // SHADOWMAP_H
