#include "main.h"
//#include"Model.h"
#include "Material.h"
#include "textures.h"
#include "AssetInstance.h"
#include"GBuffer.h"
#include"Sphere.h"
#include"PointLight.h"
#include"DirectionalLight.h"
#include"SpotLight.h"
#include"Cone.h"

int WindowHeight, WindowWidth;
SDL_Window* Window = NULL;
SDL_GLContext Context;
bool Close = true;
int main( int argc, char* argv[] )
{

	// Initialise GLFW
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	 {
		 printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		 return -1;
     }

     //Use OpenGL 4.5
     SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
     SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
	 SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 5 );

     WindowWidth = 1920;
     WindowHeight = 1080;
     Window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WindowWidth, WindowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

	if( Window == NULL )
	{
          printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
          return -1;
    }

    Context = SDL_GL_CreateContext( Window );
    if( Context == NULL )
	{
		printf( "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError() );
		return -1;
	}


	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}
	//Use Vsync(coomment to disable)
	/*if( SDL_GL_SetSwapInterval( 1 ) < 0 )
	{
		printf( "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError() );
	}*/

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// !!!!!!!!!!!!!!!!!!!!!!!test!!!!!!!!!!!!!!!!!!!

	 //Create and compile our GLSL program from the shaders
	//GLuint programID = LoadShaders( "SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader" );

	Model TestM;
          TestM.Load_Obj("test.obj", GL_STATIC_DRAW);
			Asset_instance TestInst(&TestM);
			GBuffer testGBuffer(WindowWidth,WindowHeight);


			GLuint   GBufferShader = LoadShaders("G-BUFFER.vertexshader", "G-BUFFER.fragmentshader");
			SDL_SetRelativeMouseMode(SDL_TRUE);
			//GLuint   TestShader = LoadShaders("test.vxs", "test.fms");

    GLuint LightProg = LoadShaders("./shaders/MVPmult.vxs", "./shaders/PointLight.fms");
    GLuint DLightProg = LoadShaders("./shaders/DirectionalLight.vxs", "./shaders/DirectionalLight.fms");
    GLuint SLProg     = LoadShaders("./shaders/MVPmult.vxs", "./shaders/SpotLight.fms");
    GLuint ShadowMapShader = LoadShaders("./shaders/CubeShadowMap.vxs", "./shaders/CubeShadowMap.fms");
    GLuint DShadowMapShader = LoadShaders("./shaders/ShadowMap.vxs", "./shaders/ShadowMap.fms");
    GLuint SphereProg = LoadShaders("./shaders/MVPmult.vxs");

    PointLight TestPL(LightProg, SphereProg, 1024 , true);
    TestPL.SetClampRad(4000.0f);
    TestPL.SetSourceRad(7.4f);
    TestPL.SetIntensity(vec3(500.0f,500.0f,500.0f));
    TestPL.set_position(vec3(0.0f, 190.0f, 0.0f));

    PointLight TestPL2(LightProg, SphereProg, 1024, true);
    TestPL2.SetClampRad(4000.0f);
    TestPL2.SetSourceRad(7.4f);
    TestPL2.SetIntensity(vec3(500.0f,500.0f,500.0f));
    TestPL2.set_position(vec3(200.0f, 1.0f, 0.0f));

    DirectionalLight DLight(DLightProg, 1024, true);
    DLight.set_rotation(vec3(0.1,0.0,0.0));
    DLight.SetIntensity(vec3(255.,249.,249.)/vec3(255.) * vec3(2.0f));
    DLight.SetAngle(0.1f);

    SpotLight TestSL(SLProg, SphereProg, 1024, true);
    TestSL.SetSourceRad(0.7f);
    TestSL.SetConeLength(1500.0f);
    TestSL.SetConeAngle(99.0f);
    TestSL.SetIntensity(vec3(1000.0f, 1000.0f, 1000.0f));
    TestSL.set_rotation(vec3(0.0,0.0,0.0));
    TestSL.set_position(vec3(0.0,100.0,0.0));

    vec3 Light1_pos(1000.0f,100.0f,0.0f), Light2_pos(-1000.0f,100.0f,0.0f);
    bool Light1Dir = true, Light2Dir = true;
    Uint32 LTime = 0;
    Uint32 CTime;
    Uint32 Delta, AVGDelta;

    Cone tCone(30);
    tCone.setSize(5.0f, 20.0f, 200.0f);

	do{
            CTime = SDL_GetTicks();
            Delta = CTime - LTime;
            LTime = CTime;
            AVGDelta += Delta;
            AVGDelta/= 2;
            printf("\r FPS: %f frame time(ms): %d", 1000.0f / AVGDelta, Delta);

         if(Light1_pos.x > 1000.0f || Light1_pos.x < -1000.0f){Light1Dir = !Light1Dir;}
         if(Light2_pos.x > 1000.0f || Light2_pos.x < -1000.0f){Light2Dir = !Light2Dir;}
         Light1_pos.x += (Light1Dir ? 1 : -1) * 10;
         Light2_pos.x += (Light2Dir ? 1 : -1) * 10;
         TestPL.set_position(Light1_pos);
         TestPL2.set_position(Light2_pos);

         //TestPL.SetPosition(vec3(187.0,60.0,-2.0));

		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();
		glm::mat4 ModelMatrix = glm::mat4(1.0f);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;


		/*glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDepthFunc(GL_LESS);
		glCullFace(GL_BACK);

		TestInst.SetMVP(MVP);
		 TestInst.Asset_DrawAll();
		glUseProgram(SLProg);
		GLuint MVPid = glGetUniformLocation(SLProg, "MVP");
		 glUniformMatrix4fv(MVPid, 1, GL_FALSE, &MVP[0][0]);
		tCone.draw();*/
		TestInst.SetMVP(MVP);


		//draws all models

	    testGBuffer.BindGBufferForDrawing();

	    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDepthFunc(GL_LESS);
		glCullFace(GL_BACK);

        TestInst.Asset_DrawAll();



		for(int i = 0;i < 6;i++)
        {
            TestPL.BindShadowMapForDrawing(i, ShadowMapShader);
            TestM.DrawAll();
        }

        for(int i = 0;i < 6;i++)
        {
            TestPL2.BindShadowMapForDrawing(i, ShadowMapShader);
            TestM.DrawAll();
        }

        DLight.BindShadowMapForDrawing(DShadowMapShader);
        TestM.DrawAll();

        TestSL.BindShadowMapForDrawing(DShadowMapShader);
        TestM.DrawAll();

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glViewport(0,0,WindowWidth, WindowHeight);

        TestPL.DrawLight(testGBuffer, MVP);
		TestPL2.DrawLight(testGBuffer, MVP);
		DLight.DrawLight(testGBuffer);
		TestSL.DrawLight(testGBuffer, MVP);
        testGBuffer.DrawGBuffer(0,1920,1080,GBufferShader);
        testGBuffer.DrawFinalBuffer();

		testGBuffer.CleanGBuffer();

		// Swap buffers*/
        SDL_GL_SwapWindow(Window);
	} // Check if the ESC key was pressed or the window was closed
	while( Close);

	// Cleanup VBO

	//glDeleteProgram(TestMat.GetShaderID());

	// Close OpenGL window and terminate GLFW

	return 0;
}

