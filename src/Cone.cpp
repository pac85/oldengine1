#include "Cone.h"

#define PI 3.14159265f

Cone::Cone(int _Polygons)
{
    Polygons = _Polygons;

    float step = 2 * PI / Polygons, curr_step = 0.0f;
    circle_vertices.resize(Polygons);

    for(int i = 0; i < Polygons - 1; i++)
    {
        curr_step += step;
        circle_vertices[i].x = cos(curr_step);
        circle_vertices[i].y = sin(curr_step);
    }
    circle_vertices[Polygons - 1] = circle_vertices[0];
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &up_circleVBO);
    glGenBuffers(1, &low_circleVBO);
    glGenBuffers(1, &coneVBO);
}

void Cone::setSize(float r1, float Angle, float Depth)
{
    //float r2 = 90.0f/(180.0f-Angle)*(Angle/60.0f) * Depth;
    float r2 = tan(PI / 360.0f * Angle) *  Depth;
    vector<vec3> low_circle_rv, up_circle, low_circle, cone;

    //Circles
    up_circle.resize(1+ Polygons);
    low_circle.resize(1+ Polygons);
    up_circle[0]  = vec3(0.0f);
    low_circle[0] = vec3(0.0f, 0.0f, -Depth);

    glBindVertexArray(VAO);

    //Top and bottom caps
    for(int i = 0; i < Polygons ;i++)
    {
        up_circle[i + 1] = circle_vertices[i] * r1;
        low_circle[i + 1] = circle_vertices[i] * r2 + vec3(0.0, 0.0, -Depth);
    }

    low_circle_rv.push_back(low_circle[0]);
    //The cone body
    for(int i = Polygons ;i > 0;i--)
    {
        cone.push_back(low_circle[i]);
        low_circle_rv.push_back(low_circle[i]);
        cone.push_back(up_circle[i]);
    }


    //Uploads all the data to the gpu
    glBindBuffer(GL_ARRAY_BUFFER, up_circleVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * up_circle.size(), &up_circle[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, low_circleVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * low_circle.size(), &low_circle_rv[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, coneVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * cone.size(), &cone[0], GL_STATIC_DRAW);

    //Cleaning...
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Cone::draw()
{
    glBindVertexArray(VAO);
    glEnableVertexAttribArray(0);
    //Draws top cap
    glBindBuffer(GL_ARRAY_BUFFER, up_circleVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, Polygons + 1);

    //Drwas lower cap
    glBindBuffer(GL_ARRAY_BUFFER, low_circleVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLE_FAN, 0, Polygons + 1);

    //Draws cone body
    glBindBuffer(GL_ARRAY_BUFFER, coneVBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, Polygons * 2);


    //Cleaning...
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Cone::~Cone()
{
    //dtor
}
