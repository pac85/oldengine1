#pragma once
#include"main.h"
#include"Model.h"
#include"Material.h"


class Asset_instance
{
private:
	Material * material;
	Model * model;
	mat4 MVP;
public:
	Asset_instance(Model *,Material *);
	Asset_instance(Model *);
	Model * GetModel();
	Material * getMaterial();
	void Asset_DrawAll();
	void SetMVP(mat4 MVPp);
	mat4 getMVP();

};