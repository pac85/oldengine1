#include"Model.h"

using namespace tinyobj;



void Model::Load_Obj(std::string File,GLenum DrawType)
{
	//Loads the obj in Mesh and materials vectors
	std::cout <<"loading "<< File << std::endl;
	std::string loadingErrors = LoadObj(Mesh, materials, File.c_str());
	if (!loadingErrors.empty())
	{
		std::cout << loadingErrors << std::endl;
		return;
	}
	MeshCount = Mesh.size();
	std::cout << File << " has " << MeshCount << " meshes " << std::endl;
	//for each mesh generates various buffers
	for (unsigned int mi = 0; mi < Mesh.size(); mi++)
	{
		//generates VAO
		GLuint VAOTemp;
		glGenVertexArrays(1, &VAOTemp);
		glBindVertexArray(VAOTemp);
		VAO.push_back(VAOTemp);
		glBindVertexArray(0);
		//generates VBO
		GLuint VBOTemp;
		glGenBuffers(1, &VBOTemp);
		glBindBuffer(GL_ARRAY_BUFFER, VBOTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.positions.size() * sizeof(float), &Mesh[mi].mesh.positions[0], DrawType);
		VBO.push_back(VBOTemp);
		//generates UVBuffer
		GLuint UVBTemp;
		glGenBuffers(1, &UVBTemp);
		glBindBuffer(GL_ARRAY_BUFFER, UVBTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.texcoords.size() * sizeof(float), &Mesh[mi].mesh.texcoords[0], DrawType);
		UVB.push_back(UVBTemp);
		//Generates normal buffer object
		GLuint NBOTemp;
		glGenBuffers(1, &NBOTemp);
		glBindBuffer(GL_ARRAY_BUFFER, NBOTemp);
		glBufferData(GL_ARRAY_BUFFER, Mesh[mi].mesh.normals.size() * sizeof(float), &Mesh[mi].mesh.normals[0], DrawType);
		NBO.push_back(NBOTemp);
		//generates index buffer object
		GLuint IBOTemp;
		glGenBuffers(1, &IBOTemp);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBOTemp);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Mesh[mi].mesh.indices.size() * sizeof(unsigned int), &Mesh[mi].mesh.indices[0], DrawType);
		IBO.push_back(IBOTemp);
		//unbinds buffers
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		//saves mesh's name
		Meshname.push_back(Mesh[mi].name);
		meshmap[Mesh[mi].name] = mi;
		//std::cout << File  <<"'s " << Meshname[mi] << " loaded in video memory" << std::endl;
	}
	std::cout << File << " loaded" << std::endl;
}





void Model::DrawAll(mat4 MVP, Material Material[])
{

	//for each model
	for (unsigned int mi = 0; mi < MeshCount; mi++)
	{
		Material[mi].UseMaterial();
		//sends the model view and projection matrix to the sjader
		/******************************************************/
		GLuint MVPid = glGetUniformLocation(Material[mi].GetShaderID(), "MVP");
		glUniformMatrix4fv(MVPid, 1, GL_FALSE, &MVP[0][0]);
		/******************************************************/

		//binds vao
		glBindVertexArray(VAO[mi]);


		//binds vbo uvb and nbo and sets them as 0st 1st and 2nd attributes
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[mi]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, UVB[mi]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);


		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, NBO[mi]);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO[mi]);



		glDrawElements(GL_TRIANGLES, Mesh[mi].mesh.indices.size(), GL_UNSIGNED_INT, (void*)0);

     }
	  //unbinds buffers
	  glBindVertexArray(0);
	  glBindBuffer(GL_ARRAY_BUFFER, 0);
	  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	  glDisableVertexAttribArray(0);
	  glDisableVertexAttribArray(1);
	  glDisableVertexAttribArray(2);
}

void Model::DrawAll()
{
    for (unsigned int mi = 0; mi < MeshCount; mi++)
	{

		//binds vao
		glBindVertexArray(VAO[mi]);


		//binds vbo uvb and nbo and sets them as 0st 1st and 2nd attributes
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[mi]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);


		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, UVB[mi]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);


		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, NBO[mi]);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO[mi]);



		glDrawElements(GL_TRIANGLES, Mesh[mi].mesh.indices.size(), GL_UNSIGNED_INT, (void*)0);

     }
	  //unbinds buffers
	  glBindVertexArray(0);
	  glBindBuffer(GL_ARRAY_BUFFER, 0);
	  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	  glDisableVertexAttribArray(0);
	  glDisableVertexAttribArray(1);
	  glDisableVertexAttribArray(2);
}

unsigned int Model::GetMeshCount()
{
	return MeshCount;
}

unsigned int Model::GetMaterialsCount()
{
	return materials.size();
}

int Model::GetMaterialId(unsigned int index1, unsigned int index2)
{
	return Mesh[index1].mesh.material_ids[!(index2 >= Mesh[index1].mesh.material_ids.size()) ? index2 : Mesh[index1].mesh.material_ids.size() - 1];
}

tinyobj::material_t Model::GetMaterial(unsigned int index)
{
	return materials[index];
}





