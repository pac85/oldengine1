#include "world.h"

world::world()
{
    //ctor
}

world::~world()
{
    //dtor
}


void world::add_actor(Actor * actor_to_add)
{
    WorldActor temp;
    temp.actor_pointer = actor_to_add;
    //Allows ticking by default
    temp.bAllowTick = true;
    temp.bTick = actor_to_add->bTick;
    actor_list.push_back(temp);
}

void world::tickAll(float delta_time)
{
    //Calls tick on every actor wich is allowed to tick and wich has the bTick bool set to true
    for(unsigned int i = 0;i < actor_list.size();i++)
    {
        if (actor_list[i].bTick && actor_list[i].bAllowTick)
        {
            actor_list[i].actor_pointer->tick(delta_time);
        }
    }
}


void world::RenderLoop()
{
    Uint32 idelta, last_time = 0, current_time;
    current_time = SDL_GetTicks();
    idelta = current_time - last_time;
    last_time = current_time;
    //Coverts the time from milliseconds integer to second floating point
    float delta = idelta / 1000.0;

    tickAll(delta);

    if(bRenderLoop)
        RenderLoop();
}
