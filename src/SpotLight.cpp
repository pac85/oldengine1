#include "SpotLight.h"
#define PI 3.14159265f

SpotLight::SpotLight(GLuint _SpotLightProgram, GLuint _ConeProgram, unsigned int ShadowMapRes, bool _bShadows) : BoundingCone(30)
{
    SpotLightProgram = _SpotLightProgram, ConeProgram = _ConeProgram, bShadows = _bShadows;

    if(bShadows)
    {
        lShadowMap = new ShadowMap(ShadowMapRes, false);
    }

    IntensityID = glGetUniformLocation(SpotLightProgram, "intensity");
    PositionID  = glGetUniformLocation(SpotLightProgram, "light_pos");
    SourceRadID = glGetUniformLocation(SpotLightProgram, "SourceRad");
    DirectionID = glGetUniformLocation(SpotLightProgram, "direction");
    ConeAngleCosID = glGetUniformLocation(SpotLightProgram, "coneAngleCos");
    IConeAngleCosID = glGetUniformLocation(SpotLightProgram, "IConeAngleIDCos");
    MVPid = glGetUniformLocation(ConeProgram, "MVP");
    VPMid = glGetUniformLocation(SpotLightProgram, "SMVP");
}


void SpotLight::BindShadowMapForDrawing(GLuint program)
{
    if(!bShadows){return;}

    //Creates the view matrix
    VPM = perspective(ConeAngle, 1.0f, SourceRad * 2, ConeLength) * lookAt(ATransform.get_position(), ATransform.get_position() + Direction * vec3(1.0,1.0,1.0), UpVec);

    lShadowMap->BindShadowMapForDrawing(VPM, program);
}


void SpotLight::set_position(vec3 _position)
{
    ATransform.set_position(_position);
}


void SpotLight::set_rotation(vec3 _rotation)
{
    ATransform.set_rotation(_rotation);
    //Direction is computed by multiplying the rotation quaternion by a vector witch points forwards
    Direction = ATransform.get_rotation_quat() * vec3(0.0,0.0,-1.0);
    //UpVec is computed by multiplying the rotation quaternion by a vector witch points upwards
    UpVec = ATransform.get_rotation_quat() * vec3(0.0,1.0,0.0);
}

void SpotLight::SetIntensity(vec3 _Intensity)
{
    Intensity = _Intensity;
}

void SpotLight::SetSourceRad(float _SourceRad)
{
    SourceRad = _SourceRad;
    BoundingCone.setSize(SourceRad, ConeAngle * 1.0f, ConeLength);
}

void SpotLight::SetConeLength(float _ConeLength)
{
    ConeLength = _ConeLength;
    BoundingCone.setSize(SourceRad, ConeAngle * 1.0f, ConeLength);
}

void SpotLight::SetConeAngle(float _ConeAngle)
{
    ConeAngle = _ConeAngle;
    BoundingCone.setSize(SourceRad, ConeAngle * 1.0f, ConeLength);
}

void SpotLight::SetInternalConeAngle(float _IConeAngle)
{
    IConeAngle = _IConeAngle;
    BoundingCone.setSize(SourceRad, ConeAngle * 1.0f, ConeLength);
}

void SpotLight::tick(float delta)
{

}

void SpotLight::GameStart()
{

}

void SpotLight::DrawLight(GBuffer InputGBuffer, mat4 MVP)
{
    InputGBuffer.BindGBufferForReading();
    //Disables depth buffer writing
    glDepthMask(GL_FALSE);

    //Sets up the stencil buffer as described here: http://ogldev.atspace.co.uk/
    glEnable(GL_STENCIL_TEST);
    glClear(GL_STENCIL_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glStencilFunc(GL_ALWAYS, 0, 0);

    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

    glUseProgram(ConeProgram);

    ConeMatrix = ATransform.get_rotation_matrix();
    mat4 ConeMVP = translate(MVP, ATransform.get_position()) * ConeMatrix;

    glUniformMatrix4fv(MVPid, 1, GL_FALSE, &ConeMVP[0][0]);
    BoundingCone.draw();
    /******************************************************************************************/

    glUseProgram(SpotLightProgram);

    //sends values to program
    glUniform3fv(PositionID , 1, value_ptr( ATransform.get_position() ));
    glUniform3fv(IntensityID, 1, value_ptr(Intensity));
    glUniform3fv(DirectionID, 1, value_ptr(Direction));
    glUniform1f(SourceRadID, SourceRad);
    glUniform1f(ConeAngleCosID , cos(PI / 360.0f * ConeAngle));
    glUniform1f(IConeAngleCosID, cos(PI / 360.0f * IConeAngle));

    InputGBuffer.BindGBufferForLighting(SpotLightProgram);

    glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

    //enables blend
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_ONE, GL_ONE);

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    if(bShadows)
    {
        lShadowMap->UseShadowMap(SpotLightProgram);
    }

    //sends matrices to the vertex shader so it can be rendered correctly
    glUniformMatrix4fv(MVPid, 1, GL_FALSE, &ConeMVP[0][0]);
    //sends the VPM to the fragment shader si it can calculate the coordinates in the shadow map
    glUniformMatrix4fv(VPMid, 1, GL_FALSE, &VPM[0][0]);
    //draws the cone
    BoundingCone.draw();

    glDisable(GL_BLEND);
    glCullFace(GL_BACK);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}

SpotLight::~SpotLight()
{
    //dtor
}
