#ifndef ACTOR_H
#define ACTOR_H
#include"main.h"


#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

struct Transform
{
    public:
        void set_rotation(vec3 _rotation);
        void set_position(vec3 _position);

        vec3 get_rotation();
        quat get_rotation_quat();
        mat4 get_rotation_matrix();
        vec3 get_position();
        mat4 get_transform();

    private:
        //rotation is in euler angles
        vec3 rotation, position;
        mat4 MTransform;
        quat QRotation;
        //needs to be called every time rotation or position is chamged to keep the matrix in sync
        void UpDate_MTransform();
};

class Actor
{
    public:
        Actor();
        virtual ~Actor();

        //events
        virtual void tick(float delta) = 0;      //called every frame
        virtual void GameStart() = 0;            //called when the game starts

        bool bTick;                              //if true tick() will be called every frame

    protected:
        Transform ATransform;

    private:

};

#endif // ACTOR_H
