#include"main.h"
#include"tiny_obj_loader.h"
#include"textures.h"
#include"Material.h"

class Model
{
private:


	mat4 Model_Matrix;
	unsigned int MeshCount;
	std::vector<tinyobj::shape_t> Mesh;
	std::vector<tinyobj::material_t> materials;
	std::vector<GLuint> VBO, UVB, NBO, VAO, IBO;
	std::vector<std::string> Meshname;
	std::map<std::string, unsigned int> meshmap;

public:

	GLuint ShaderID;

	// Model_Matrix setter
	void Load_Obj(std::string File, GLenum DrawType);
	void DrawAll(mat4 MVP, Material Material[]);
	void DrawAll();
	unsigned int GetMeshCount();
	unsigned int GetMaterialsCount();
	int  GetMaterialId(unsigned int index1,unsigned int index2);
	tinyobj::material_t GetMaterial(unsigned int index);



};



