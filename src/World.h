#ifndef WORLD_H
#define WORLD_H
#include"main.h"
#include"Actor.h"
#include"PointLight.h"
#include"SpotLight.h"
#include"DirectionalLight.h"


struct WorldActor
{
    Actor * actor_pointer;  //The Actor pointer
    bool bTick;     //Tick only if true
    bool bAllowTick; //If false tick() will not be called even if bTick is true
};

class world
{
    public:
        world();
        virtual ~world();

        void add_actor(Actor * actor_to_add); //adds an actor to the world
        void start_simulation();              //starts all the threads for each loop
        void end_simulation();                //ends all the threads created by previous function

    protected:

    private:
        //Lists of actor, lights get special lists becouse the world class has to render their shadow maps
        vector<WorldActor> actor_list;
        vector<PointLight> PointLights_list;
        vector<SpotLight>  SpotLights_list;
        vector<DirectionalLight> DirectionalLights_list;

        void tickAll(float delta_time);      //Calls tick for every object

        bool bRenderLoop;                    //If true all the rendering thread will loop
        void RenderLoop();                   //the function of the main rendering thread

};

#endif // WORLD_H
