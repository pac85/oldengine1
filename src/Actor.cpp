#include "Actor.h"

Actor::Actor()
{
    //ctor
}

Actor::~Actor()
{
    //dtor
}


void Transform::UpDate_MTransform()
{
    QRotation = quat(rotation);
    mat4 M_rotation = toMat4(QRotation);
    MTransform      = translate(M_rotation, position);
}

void Transform::set_rotation(vec3 _rotation)
{
    rotation = _rotation;
    UpDate_MTransform();
}

void Transform::set_position(vec3 _position)
{
    position = _position;
    UpDate_MTransform();
}

vec3 Transform::get_rotation()
{
    return rotation;
}

quat Transform::get_rotation_quat()
{
    return QRotation;
}

mat4 Transform::get_rotation_matrix()
{
    return toMat4(QRotation);
}

vec3 Transform::get_position()
{
    return position;
}

mat4 Transform::get_transform()
{
    return MTransform;
}
