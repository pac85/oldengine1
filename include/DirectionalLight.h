#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H
#include"main.h"
#include"GBuffer.h"
#include"ShadowMap.h"
#include"Actor.h"


class DirectionalLight : Actor
{
    public:
        DirectionalLight(GLuint , unsigned int, bool);
        void BindShadowMapForDrawing(GLuint);
        void DrawLight(GBuffer InputGBuffer);
        void SetIntensity(vec3 Int);
        void set_rotation(vec3 _rotation);
        void SetAngle(float Angle);
        virtual ~DirectionalLight();

    protected:

    private:
        bool bShadows;
        unsigned int ShadowMapRes;
        GLuint DirLightProgram, DirectionID, IntensityID, VPMIndexID, AngleID;
        ShadowMap * lShadowMap;
        vec3 Direction, intensity;
        float source_angle;
        mat4 VPM;

        void tick(float delta);
        void GameStart();
};

#endif // DIRECTIONALLIGHT_H
